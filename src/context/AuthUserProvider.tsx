import React, {PropsWithChildren, useMemo, useState} from 'react';

export type Auth = {
  isAuth?: boolean;
  setAuth?: (isAuth: boolean) => void;
  userName?: string;
  setUserName?: (userName: string) => void;
};

export const AuthUserContext = React.createContext<Auth>({
  isAuth: false,
});

export const AuthUserProvider: React.FC<PropsWithChildren<React.ReactNode>> = ({
  children,
}) => {
  const [isAuth, setAuth] = useState<boolean>();
  const [userName, setUserName] = useState<string>();

  const authValue = useMemo(
    () => ({isAuth, setAuth, userName, setUserName}),
    [isAuth, setAuth, userName, setUserName],
  );

  return (
    <AuthUserContext.Provider value={authValue}>
      {children}
    </AuthUserContext.Provider>
  );
};
