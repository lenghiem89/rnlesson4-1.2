import {Detail} from '../screen/Detail';
import Home from '../screen/Home';
import About from '../screen/Setting';
import BottomTabStack from './BottomTabStack';
import TabIcon from './TabIcon';

export type Screen = {
  name: string;
  component: any;
  options?: any;
};

export const MAIN_STACK_SCREEN: Screen[] = [
  {
    name: 'MainTab',
    component: BottomTabStack,
  },
  {
    name: 'Detail',
    component: Detail,
  },
];

export const BOTTOM_TAB_STACK_SCREEN: Screen[] = [
  {
    name: 'Home',
    component: Home,
    options: {
      tabBarIcon: ({color, size}) => {
        return TabIcon('home', color, size);
      },
    },
  },
  {
    name: 'About',
    component: About,
    options: {
      tabBarIcon: ({color, size}) => {
        return TabIcon('information', color, size);
      },
    },
  },
];
