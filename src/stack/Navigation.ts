import {
  BottomTabNavigationProp,
  BottomTabScreenProps,
} from '@react-navigation/bottom-tabs';
import {StackNavigationProp, StackScreenProps} from '@react-navigation/stack';

type HomeProps = {
  userId: number;
};
type DetailsProps = {
  id: number;
};

export type AuthStackParamList = {
  Login: undefined;
};

export type MainStackParamList = {
  Home: HomeProps | undefined;
  Detail: DetailsProps | undefined;
  Setting: undefined;
};

export type MainStackNavigation = StackNavigationProp<MainStackParamList>;
export type S = keyof MainStackParamList;
export type MainStackScreenProps<RouteName extends S> = StackScreenProps<
  MainStackParamList,
  RouteName
>;

// for bottom tab navigation
export type TabStackParamList = {
  MainTab: undefined;
};

export type T = keyof TabStackParamList;
export type TabStackScreenProps<RouteName extends T> = BottomTabScreenProps<
  TabStackParamList,
  RouteName
>;

export type TabStackNavigation = BottomTabNavigationProp<TabStackParamList>;
