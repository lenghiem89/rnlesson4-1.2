import {NavigationContainer} from '@react-navigation/native';
import React, {useContext} from 'react';
import {AuthUserContext} from '../context/AuthUserProvider';
import LoginStack from './LoginStack';
import MainStack from './MainStack';

export const AppRouter = () => {
  const auth = useContext(AuthUserContext);
  return (
    <NavigationContainer>
      {auth.isAuth ? <MainStack /> : <LoginStack />}
    </NavigationContainer>
  );
};
