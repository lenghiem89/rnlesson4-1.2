import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Login from '../screen/Login';
import {AuthStackParamList} from './Navigation';

const Stack = createStackNavigator<AuthStackParamList>();
const LoginStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={Login} />
    </Stack.Navigator>
  );
};

export default LoginStack;
