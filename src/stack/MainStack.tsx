import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {MainStackParamList, TabStackParamList} from './Navigation';
import {MAIN_STACK_SCREEN} from './Screen';
const Stack = createStackNavigator<TabStackParamList & MainStackParamList>();
const MainStack = () => {
  return (
    <Stack.Navigator>
      {MAIN_STACK_SCREEN.map(item => (
        <Stack.Screen
          key={item.name}
          name={item.name as keyof MainStackParamList}
          component={item.component}
          options={item.options}
        />
      ))}
    </Stack.Navigator>
  );
};

export default MainStack;
