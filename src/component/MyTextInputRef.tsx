import React, {useState} from 'react';
import {TextInput, TextInputProps} from 'react-native';

type MyTextInputProps = TextInputProps & {};

export type MyTextInputHandle = {
  getText: () => string | undefined;
  setTextValue: (textValue: string | undefined) => void;
};
//cach 1
const MyTextInput: React.ForwardRefRenderFunction<
  MyTextInputHandle,
  MyTextInputProps
> = (props, forwardedRef) => {
  const [text, setText] = useState<string>();
  React.useImperativeHandle(forwardedRef, () => ({
    getText() {
      return text;
    },
    setTextValue: textValue => {
      setText(textValue);
    },
  }));

  return <TextInput value={text} onChangeText={setText} {...props} />;
};

export default React.forwardRef(MyTextInput);

//cach 2
export const MyTextInputV2 = React.forwardRef<
  MyTextInputHandle,
  MyTextInputProps
>((props, forwardedRef) => {
  const [text, setText] = useState<string>();
  React.useImperativeHandle(forwardedRef, () => ({
    getText() {
      return text;
    },
    setTextValue: textValue => {
      setText(textValue);
    },
  }));

  return <TextInput value={text} onChangeText={setText} {...props} />;
});

//cach 3
export const MyTextInputV3 = React.forwardRef(
  (
    props: React.PropsWithChildren<TextInputProps>,
    forwardedRef: React.ForwardedRef<MyTextInputHandle>,
  ) => {
    const [text, setText] = useState<string>();
    React.useImperativeHandle(forwardedRef, () => ({
      getText() {
        return text;
      },
      setTextValue: textValue => {
        setText(textValue);
      },
    }));

    return <TextInput value={text} onChangeText={setText} {...props} />;
  },
);
