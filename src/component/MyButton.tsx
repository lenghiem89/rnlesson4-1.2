import React from 'react';
import {
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle,
} from 'react-native';

export type Props = TouchableOpacityProps & {
  buttonText?: string;
  buttonStyle?: ViewStyle;
  textStyle?: TextStyle;
};

export const MyButton: React.FC<Props> = ({
  textStyle = styles.textStyle,
  buttonText = 'Button',
  buttonStyle = styles.button,
  ...others
}) => {
  return (
    <TouchableOpacity style={buttonStyle} {...others}>
      <Text style={textStyle}>{buttonText}</Text>
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  button: {
    padding: 20,
    marginHorizontal: 10,
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    backgroundColor: 'purple',
  },
  textStyle: {
    // margin: 20,
    fontWeight: '700',
    textAlign: 'center',
    color: 'red',
  },
});

export default MyButton;
