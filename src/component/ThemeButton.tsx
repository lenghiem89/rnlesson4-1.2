import React from 'react';
import useThemeStyleContext from '../hooks/useThemStyleContext';
import MyButton, {Props, styles} from './MyButton';

export const ThemeButton: React.FC<Props> = props => {
  const theme = useThemeStyleContext();
  return (
    <MyButton
      style={[styles.button, theme.buttonBackgroundColor]}
      textStyle={theme.textColor}
      {...props}
    />
  );
};

export default ThemeButton;
