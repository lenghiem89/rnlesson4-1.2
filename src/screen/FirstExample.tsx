/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {Colors, Header} from 'react-native/Libraries/NewAppScreen';

const First = () => {
  return <Text style={styles.first}>My first component</Text>;
};

type SecondProps = {
  text: string;
  color: string;
};
const Second: React.FC<SecondProps> = props => {
  return (
    /** khong nen dung */
    // eslint-disable-next-line react-native/no-inline-styles
    <View style={{backgroundColor: 'gray'}}>
      <Text style={[styles.second, {color: props.color}]}>{props.text}</Text>
    </View>
  );
};

const Three: React.FC<SecondProps> = ({text, color}) => {
  return (
    <View>
      <Text style={[styles.second, {color: color}]}>{text}</Text>
    </View>
  );
};

const Three2: React.FC<SecondProps> = ({text: title, color}) => {
  return (
    <View>
      <Text style={[styles.second, {color: color}]}>{title}</Text>
    </View>
  );
};

const FirstExample = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  console.log('App render');
  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <First />
        <Second text="My second component" color="red" />
        <Three text="My Three component" color="purple" />
        <Three2 text="My Three2 component" color="purple" />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  first: {
    marginTop: 8,
    fontSize: 30,
    fontWeight: '400',
    color: 'green',
  },
  second: {
    fontSize: 20,
    color: 'gray',
  },
});

export default FirstExample;
