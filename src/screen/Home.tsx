import React, {useContext} from 'react';
import {Button, View, Text} from 'react-native';
import ThemeButton from '../component/ThemeButton';
import {AuthUserContext} from '../context/AuthUserProvider';
import useThemeContext from '../hooks/useThemeContext';
import useThemeStyleContext from '../hooks/useThemStyleContext';
import {MainStackScreenProps} from '../stack/Navigation';

const Home: React.FC<MainStackScreenProps<'Home'>> = ({navigation, route}) => {
  const theme = useThemeStyleContext();
  const {setDefaultTheme, setBlackTheme} = useThemeContext();
  const userName = useContext(AuthUserContext);
  return (
    <View style={theme.backgroundStyle}>
      <Text style={theme.textColor}>Home Screen</Text>
      <Text>Xin chao {userName.userName}! </Text>
      {route?.params && (
        <Text style={theme.textColor}>
          With received params : {route?.params?.userId}
        </Text>
      )}
      <Button
        title="Detail Screen"
        color={theme?.buttonBackgroundColor?.backgroundColor}
        onPress={() => {
          navigation.navigate('Detail', {
            id: Math.floor(Math.random() * 100),
          });
        }}
      />
      <ThemeButton buttonText="Change Black Theme" onPress={setBlackTheme} />
      <ThemeButton
        buttonText="Change Default Theme"
        onPress={setDefaultTheme}
      />
    </View>
  );
};
export default Home;
